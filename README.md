# Home Automation Project
## Get started

### Prerequisites
- Download Raspberry Pi OS Lite
    - 32-bits is OK for the clients, but 64-bits is required for the server to be able to use MongoDB
- Download Raspberry Pi Imager
- Write the OS to a SD-card with Raspberry Pi Imager
- Enable SSH
    - Add an empty file named ```ssh``` in the root of the SD-card
    - It's possibly to SSH to the Raspberry with Putty for examaple by using ```raspberrypi.local``` as address
    - Default username is ```pi``` and default password is ```raspberry```

### Configuration
#### Setup static IP address
Add the following in ```/etc/dhcpd.conf```
```bash
interface eth0
static ip_address=192.168.1.200
static routers=192.168.1.1
static domain_name_servers=192.168.1.1 8.8.8.8
```

#### Setup WiFi
Add SSID and password to ```/etc/wpa_supplicant/wpa_supplicant.conf```
```bash
network={
   ssid="NETWORK_NAME"
   psk="PASSWORD"
}
```
After the file has been saved, run ```wpa_cli -i wlan0 reconfigure```

#### Update password
Use ```passwd``` to set a new password

#### Install screen
```sudo apt install screen```

#### Add services to start after boot
Edit ```crontab``` to execute the start-scripts after each reboot.

Edit crontab by typing ```crontab -e``` and then add the script you want to execute after boot at the end of the file.
For example to execute the ```startServer.sh``` after reboot you add this to the end of the file ```@reboot /home/pi/startServer.sh```

#### Install MongoDB (only for Raspberry acting as server)
```bash
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt-get update
sudo apt-get install mongodb-org

sudo systemctl start mongod
sudo systemctl enable mongod
```

### Throubleshooting
- Error-message when running ```apt-get update```:
    - ```E: Release file for `X' is not valid yet (invalid for another TIME)```
        - Then try: ```apt-get -o Acquire::Check-Valid-Until=false -o Acquire::Check-Date=false update```
        - Or try to update the time using ```ntp```

## The way we work
- The project is in a shell-structure. Which means there is a shell that contains all projectes as sub-modules.
    - Update all submodule with ```git submodule update --init --recursive```
- Pushes to remote ```master``` and ```dev``` will trigger builds in Bitbucket Pipeline.
- Adding a tag to a commit in the shell will trigger a deploy to the deployserver.
- Download and install the latest version from the deployserver
    - Run ```./updateServer.sh``` for example to install the latest Server application
- Start an application
    - Run ```./startServer.sh``` to start the Server application
    
## IP addresses
| Application        | WiFi          | Ethernet      |
| -------------------|-------------- | ------------- |
| Server             |      N/A      | 192.168.1.200 |
| Client1            | 192.168.1.202 | 192.168.1.201 |

## Ports
| Application        | Port | External access |
| -------------------|------|-----------------|
| Server             | 1337 |       No        |
| DeploymentServer   | 9999 |       Yes       |
| Web                | 8000 |       Yes       |
| Database.Api       | 5001 |    Not sure     |
